package nl.utwente.di;

import java.util.HashMap;

public class Quoter {
	private HashMap<String, Double> quotes;
	public Quoter() {
		this.quotes = new HashMap<>();
		this.quotes.put("1", 10.0);
		this.quotes.put("2", 45.0);
		this.quotes.put("3", 20.0);
		this.quotes.put("4", 35.0);
		this.quotes.put("5", 50.0);
	}

	public double getBookPrice(String isbn) {
		return this.quotes.getOrDefault(isbn, 0.0);
	}
}
