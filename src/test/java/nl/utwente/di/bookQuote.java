package nl.utwente.di;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class bookQuote {
	@Test
	public void testCelcius() throws Exception {
		Quoter quoter = new Quoter();
		double price = quoter.getBookPrice("1");
		Assertions.assertEquals(10.0, price, 0.0, "Price of book 1");

	}
}
